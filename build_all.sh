#!/bin/bash

BASEDIR="$( cd "$( dirname "$(realpath $BASH_SOURCE)" )" && pwd )"
cd $BASEDIR

mkdir -pv zmusic_build/zmusic/build
mkdir -pv gzdoom_build/gzdoom/build



$BASEDIR/build_zmusic.sh
$BASEDIR/build_gzdoom.sh
