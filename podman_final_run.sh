#!/bin/bash
BASEDIR="$( cd "$( dirname "$(realpath $BASH_SOURCE)" )" && pwd )"
cd $BASEDIR
podman run \
--rm \
--init \
-v "$BASEDIR":"/home/podman" \
-v "$BASEDIR/zmusic_build":"/home/podman/zmusic_build" \
-v "$BASEDIR/staging_dist":"/home/podman/staging_dist" \
-v "$BASEDIR/gzdoom_build":"/home/podman/gzdoom_build" \
-e HOME="/home/podman" \
-v /etc/passwd:/etc/passwd:ro \
-v /etc/group:/etc/group:ro \
-h "$(hostname)" \
-v /tmp:/tmp \
registry.gitlab.steamos.cloud/steamrt/sniper/sdk:latest \
/home/podman/build_all.sh


#podman finished running. Copy what we need to final directory
mkdir -p final_dist
cp -r gzdoom_build/gzdoom/build/fm_banks final_dist/
cp -r gzdoom_build/gzdoom/build/soundfonts final_dist/
cp gtaging_dist/lib/libzmusic.so.1.1.11 final_dist/libzmusic.so
cp gzdoom_build/gzdoom/build/gzdoom.pk3 final_dist
cp dist_scripts/gzdoom final_dist/

