export CMAKE_PREFIX_PATH=~/staging_dist/;

a='' && [ "$(uname -m)" = x86_64 ] && a=64
c="$(lscpu -p | grep -v '#' | sort -u -t , -k 2,4 | wc -l)" ; [ "$c" -eq 0 ] && c=1
cd ~/gzdoom_build/gzdoom/build &&
rm -f output_sdl/liboutput_sdl.so &&
cmake .. -DCMAKE_BUILD_TYPE=Release &&
make -j$c
