# I Am Sakuya Linux Staging Area



## What am I looking at?

This is everything you need in order to:
1. Succesfully compile gzdoom on steam-runtime.
2. Mimic the Windows-like properties on linux.

## IMPORTANT NOTICE

I was unable to compile gzdoom on scout (gzdoom requires linux kernel 3.12, due to its extended mmap struct). So I chose sniper. Until Steam has support for diffrent Steam Runtimes, please advise end users to install and run through the Steam Runtime Sniper tool.

## How to run?
First pull this repository with submodules either with `git clone --recursive` or `git submodule update --init`

This requires an existing Linux system to be installed. Tested on manjaro. WSL should work, although it is untested.
Linux installation requires `bash`, `docker` and `podman`.
Once this stuff is installed, run `podman_final_run.sh` file in terminal.

Once finished, the prepared linux gzdoom distribution will be at `final_dist` directory.
